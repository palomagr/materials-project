# Materials Project

**Regional materials for Fabrication: Measurement, Instrumentalization and Development of materials with regional resources.**


![img software](instron_test_1/DSC06047-min.JPG)


This is a project for developing materials with biological and local resources in the context of a fablab and with the goal of working with digital fabrication processes. 

# ToDo 

## Catalogue and Document Materials Processes

Catalogue a material by its components and production processes.

## Measure Mechanical Properties

Tensile Strength
Compression Load witn 3 point bend test

## Instron Documentation

Tensile Strength
Compression Load witn 3 point bend test

![img software](instron_test_1/IMG_20190227_180530.jpg )


